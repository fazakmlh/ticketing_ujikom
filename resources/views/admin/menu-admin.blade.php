<!-- Sidebar Menu -->
<ul class="sidebar-menu">
  <li class="header">HEADER</li>
  <!-- Optionally, you can add icons to the links -->
  <li class="active"><a href="{{url('/admin/')}}"><i class="fa fa-link"></i> <span>Dashboard</span></a></li>
  {{-- <li class="treeview">
    <a href="#"><i class="fa fa-train"></i> <span>Transportasi</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="{{url('admin/transportation_type')}}"><i class="fa fa-tag"></i> Tipe Transportasi</a></li>
      <li><a href="{{url('admin/transportation')}}"><i class="fa fa-train"></i> Transportasi</a></li>
    </ul>
  </li> --}}
  <li><a href="{{url('/admin/transportation')}}"><i class="fa fa-train"></i> <span>Transportasi</span></a></li>
  <li><a href="{{url('/admin/terminal')}}"><i class="fa fa-terminal"></i> <span>Terminal</span></a></li>
  <li><a href="{{url('/admin/rute')}}"><i class="fa fa-road"></i> <span>Rute</span></a></li>
</ul>
<!-- /.sidebar-menu -->
