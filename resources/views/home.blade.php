@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-default">
                <div class="card-header">Tipe Transfortasi</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <a href="{{url('/train')}}" class="btn btn-primary">Kereta Api</a>
                    <a href="{{url('/airport')}}" class="btn btn-success">Pesawat Terbang</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
