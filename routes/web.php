<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::prefix('admin')->namespace('Admin')->group(function(){
  Route::get('/', 'HomeController@index');

  // Transaction Type
  Route::get('transportation_type', 'TransportationTypeController@index');
  Route::post('transportation_type', 'TransportationTypeController@store');
  Route::patch('transportation_type', 'TransportationTypeController@update');
  Route::delete('transportation_type', 'TransportationTypeController@delete');
  // End Transaction Type

  // Transportation
  Route::get('transportation', 'TransportationController@index');
  Route::get('/transportation/add', 'TransportationController@add');
  Route::get('/transportation/{transportation}', 'TransportationController@show');
  Route::get('/transportation/{transportation}/edit', 'TransportationController@edit');
  Route::post('/transportation', 'TransportationController@store');
  Route::patch('/transportation', 'TransportationController@update');
  Route::delete('/transportation', 'TransportationController@delete');
  // End Transportation

  // Seat
  Route::post('seat', 'SeatController@store');
  Route::delete('seat', 'SeatController@delete');
  // End Seat

  // Terminal
  Route::get('terminal', 'TerminalController@index');
  Route::post('terminal', 'TerminalController@store');
  Route::patch('terminal', 'TerminalController@update');
  Route::delete('terminal', 'TerminalController@delete');
  // End Terminal

  // Rute
  Route::get('rute', 'RuteController@index');
  Route::get('rute/add', 'RuteController@add');
  Route::get('rute/{rute}/edit', 'RuteController@edit');
  Route::post('rute', 'RuteController@store');
  Route::patch('rute', 'RuteController@update');
  Route::delete('rute', 'RuteController@delete');
  // End Rute

});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/train', 'TrainController@index');
Route::get('/train/search', 'TrainController@search');

Route::get('booking', 'BookingController@index');
Route::post('booking', 'BookingController@index');
Route::post('booking/seat', 'BookingController@seat');
Route::post('booking/confirm-booking', 'BookingController@confirm');
Route::post('booking/make-booking', 'BookingController@make');
