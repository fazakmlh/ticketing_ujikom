<?php

use Faker\Generator as Faker;

$factory->define(App\Rute::class, function (Faker $faker) {
    return [
        'transportation_id' => function(){
          return factory(App\Transportation::class)->create()->id;
        },
        'depart_at'
    ];
});
