<?php

use Faker\Generator as Faker;

$factory->define(App\Transportation::class, function (Faker $faker) {
    return [
        'transportation_type_id' => function(){
          return factory(App\TransportationType::class)->create()->id;
        },
        'code'       => function(){
          $type = strtolower(str_random(5));
          $code = rand(10, 30);
          return $type."_".$code;
        },
        'description' => $faker->name
    ];
});
