<?php

use Illuminate\Database\Seeder;

class TransportationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $transportation_types = App\TransportationType::all();

        $transportation_types->each(function($transportation_type, $index){
          factory(App\Transportation::class, 10)->create(['transportation_type_id' => $transportation_type->id]);
        });
    }
}
