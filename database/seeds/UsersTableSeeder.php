<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
          'fullname'       => 'Salim Arizi',
          'email'          => 'salim@arizi.com',
          'password'       => bcrypt('salim123'),
          'level'          => 'user',
          'remember_token' => str_random(10)
        ]);

        App\User::create([
          'fullname'       => 'Salim Admin',
          'email'          => 'salim@admin.com',
          'password'       => bcrypt('salimadmin'),
          'level'          => 'admin',
          'remember_token' => str_random(10)
        ]);
    }
}
