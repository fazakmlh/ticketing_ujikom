<?php

use Illuminate\Database\Seeder;

class RutesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $transportation = App\Transportation::all();

        $transportation->each(function($transportation, $index){
          factory(App\Rute::class, 10)->create(['transportation_id' => $transportation->id]);
        });
    }
}
