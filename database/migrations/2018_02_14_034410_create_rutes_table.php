<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rutes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transportation_id')->unsigned()->index();
            $table->time('depart_at');
            $table->integer('rute_from')->unsigned()->index();
            $table->integer('rute_to')->unsigned()->index();
            $table->decimal('price',10,0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('transportation_id')
                  ->references('id')
                  ->on('transportations')
                  ->onDelete('cascade');

            $table->foreign('rute_from')
                  ->references('id')
                  ->on('terminals')
                  ->onDelete('cascade');

            $table->foreign('rute_to')
                  ->references('id')
                  ->on('terminals')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rutes');
    }
}
