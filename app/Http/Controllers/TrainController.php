<?php

namespace App\Http\Controllers;

use App\Rute;
use App\Terminal;
use App\Transportation;
use Illuminate\Http\Request;

class TrainController extends Controller
{
    public function index()
    {
        $terminals = Terminal::all();
        return view('train.index', compact('terminals'));
    }

    public function search(Request $r)
    {

        $rutes = Rute::where([
                      ['rute_from', $r->rute_from],
                      ['rute_to', $r->rute_to]
                    ])
                    ->get();

        $data = [
          'date'  => $r->date,
          'rutes' => $rutes,
          'adult' => $r->adult,
          'infant'=> $r->infant
        ];
        return view('train.lists', $data);
    }
}
