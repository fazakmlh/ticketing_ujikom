<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Session;
use App\Terminal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TerminalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request,$next){
          if (Auth::user()->level != 'admin') {
            return redirect('home');
          }
          return $next($request);
        });
    }

    public function index()
    {
        $terminals = Terminal::all();
        return view('admin.terminal', compact('terminals'));
    }

    public function store(Request $r)
    {
        Terminal::create([
          'name' => $r->name,
          'city' => $r->city
        ]);

        Session::flash('status', 'Anda berhasil menambah terminal');
        return redirect(url()->previous());
    }

    public function update(Request $r)
    {
        Terminal::find($r->terminal_id)->update([
          'name' => $r->edit_name,
          'city' => $r->edit_city
        ]);

        Session::flash('status', 'Anda berhasil mengedit terminal');
        return redirect(url()->previous());
    }

    public function delete(Request $r)
    {
        Terminal::find($r->terminal_id)->delete();

        Session::flash('status', 'Anda berhasil menghapus tipe terminal');
        return redirect(url()->previous());
    }
}
