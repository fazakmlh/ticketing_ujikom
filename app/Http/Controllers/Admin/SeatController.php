<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Session;
use App\Seat;
use App\Transportation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SeatController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request,$next){
          if (Auth::user()->level != 'admin') {
            return redirect('home');
          }
          return $next($request);
        });
    }

    public function store(Request $r)
    {
        $transportation = Transportation::find($r->transportation_id);
        $start = $transportation->seats->count();
        $end   = $start + $r->seat_quantity;

        for ($i = $start; $i < $end ; $i++) {
          $seat_code = '999'.$transportation->id.'SEAT_'.$i;
          $check = Seat::where('seat_code', $seat_code)->first();

          if ($check) {
            $seat_code = '999'.$transportation->id.'SEAT_'.($i+1);
          }

          Seat::create([
            'transportation_id' => $transportation->id,
            'seat_code'         => $seat_code
          ]);
        }

        Session::flash('status', 'Anda berhasil menambah kursi');
        return redirect(url()->previous());
    }

    public function delete(Request $r)
    {
        Seat::find($r->seat_id)->delete();

        Session::flash('status', 'Anda berhasil menghapus kursi');
        return redirect(url()->previous());
    }
}
