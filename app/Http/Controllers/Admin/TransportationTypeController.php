<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Session;
use App\TransportationType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransportationTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request,$next){
          if (Auth::user()->level != 'admin') {
            return redirect('home');
          }
          return $next($request);
        });
    }

    public function index()
    {
        $transportation_types = TransportationType::all();
        return view('admin.transportation.type', compact('transportation_types'));
    }

    public function store(Request $r)
    {
        TransportationType::create([
          'description' => $r->name
        ]);

        Session::flash('status', 'Anda berhasil menambah tipe transportasi');
        return redirect(url()->previous());
    }

    public function update(Request $r)
    {
        TransportationType::find($r->transportation_type_id)->update([
          'description' => $r->edit_name
        ]);

        Session::flash('status', 'Anda berhasil mengedit tipe transportasi');
        return redirect(url()->previous());
    }

    public function delete(Request $r)
    {
        TransportationType::find($r->transportation_type_id)->delete();

        Session::flash('status', 'Anda berhasil menghapus tipe transportasi');
        return redirect(url()->previous());
    }
}
