<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    protected $admin;
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request,$next){
          if (Auth::user()->level != 'admin') {
            return redirect('home');
          }
          return $next($request);
        });
    }

    public function index()
    {
        return view('admin.home');
    }
}
