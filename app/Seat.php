<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Seat extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function transportation()
    {
        return $this->belongsTo(Transportation::class);
    }

    public function booking()
    {
        return $this->belongsTo(Reservation::class);
    }
}
