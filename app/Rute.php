<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Rute extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function transportation()
    {
        return $this->belongsTo(Transportation::class);
    }

    public function origin()
    {
        return $this->belongsTo(Terminal::class,'rute_from','id');
    }

    public function destination()
    {
        return $this->belongsTo(Terminal::class,'rute_to','id');
    }

    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }
}
